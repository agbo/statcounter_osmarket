#!/bin/bash

WEBPAGE=$(curl -s http://gs.statcounter.com/os-market-share/all/)

COUNTRY_LIST=$(echo $WEBPAGE | sed 's/<a/\
\<a/g' | grep -v yearShortcut | grep os-market-share  | sed 's/.*all\///' | \
 cut -d\" -f1 | grep -v \< | tail -n +9)

for i in $COUNTRY_LIST; do 
 printf "%s:%s\n" "$(curl -s http://gs.statcounter.com/os-market-share/desktop/$i | \
  grep -2 Linux | head -4 | tail -1| sed -n '/^$/!{s/<[^>]*>//g;p;}' |\
  sed -r 's/\s+//g')" "$i" | tee -a /tmp/linux_by_country 
done

printf "\n\n\n\n\n%s\n%s\n" "Now the sorted list" "___________________"

sort -nr /tmp/linux_by_country | tee -a /tmp/linux_by_country_sorted

